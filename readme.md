To start the json server: 
1. yarn install
2. yarn json-server --watch db.json --routes routes.json
3. try an api call, visit http://localhost:3000/domain

Json server documentation: https://www.npmjs.com/package/json-server
